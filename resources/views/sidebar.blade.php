<?php
    $level = auth()->user()->level;
?>

<div class="p-3 d-flex flex-column d-print-none" style="width: 280px" id="navigation-bar">
    <a href="#" class="d-flex align-items-center mb-3 text-decoration-none">
        <img src="/img/logo.png" style="width: 40px">
        <span class="fs-4 ms-2">APLIKASI</span>
    </a>
    <ul class="nav flex-column mb-auto">
        @if($level != 'student')
            <li><a href="/{{ $level }}/users" class="nav-link">User</a></li>
            <li><a href="/{{ $level }}/students" class="nav-link">Siswa</a></li>
        @endif
        <li><a href="/{{ $level }}/bills" class="nav-link">Tagihan</a></li>
        <li><a href="/{{ $level }}/payments" class="nav-link">Pembayaran</a></li>
    </ul>
    <div class="dropdown">
        <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
            <i class="bi bi-person-circle" style="font-size: 30px"></i>
        </a>
        <ul class="dropdown-menu">
            <li><a href="#" class="dropdown-item">Halo, {{ auth()->user()->username }}</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a href="/logout" class="dropdown-item">Log out</a></li>
        </ul>
    </div>
</div>

