<html>

<head>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>

<body>
    <main class="d-flex" style="height: 100vh">
        @include('header')

        @include('sidebar')

        @yield('content')

        @include('footer')
    </main>
    <script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>
