@extends('app')

@section('content')
    <div class="container">
        <h1>Tambah Siswa</h1>
        <form action="/admin/students" method="POST">
            @csrf
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="name" class="form-label">Nama</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="col-3 mb-3">
                    <label for="nisn" class="form-label">NISN</label>
                    <input type="text" class="form-control" id="nisn" name="nisn">
                </div>
                <div class="col-3 mb-3">
                    <label for="nis" class="form-label">NIS</label>
                    <input type="text" class="form-control" id="nis" name="nis">
                </div>
                <div class="col-3 mb-3">
                    <label for="address" class="form-label">Alamat</label>
                    <input type="text" class="form-control" id="address" name="address">
                </div>
                <div class="col-3 mb-3">
                    <label for="phone" class="form-label">No. Telepon</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                </div>
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                </div>
                <div class="col-3 mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="col-3 mb-3">
                    <label class="form-label">Level</label>
                    @foreach (['admin', 'staff', 'student'] as $item)
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="level" value="{{ $item }}">
                            <label class="form-check-label">{{ $item }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
    </div>
@endsection
