@extends('app')

@section('content')
    <div class="container">
        <h1>Tambah Tagihan</h1>
        <form action="/admin/bills" method="POST">
            @csrf
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="amount" class="form-label">Jumlah</label>
                    <input type="number" class="form-control" id="amount" name="amount">
                </div>
                <div class="col-3 mb-3">
                    <label for="month" class="form-label">Bulan</label>
                    <input type="number" class="form-control" id="month" name="month">
                </div>
                <div class="col-3 mb-3">
                    <label for="year" class="form-label">Tahun</label>
                    <input type="number" class="form-control" id="year" name="year">
                </div>
                <div class="col-3 mb-3">
                    <label class="form-label">Student ID</label>
                    <select name="student_id" class="form-select">
                        @foreach ($student_list as $student)
                            <option value="{{ $student->id }}">{{ $student->id }} - {{ $student->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
    </div>
@endsection
