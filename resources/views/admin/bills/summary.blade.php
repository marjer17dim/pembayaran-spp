@extends('app')

@section('content')
    <div class="container">
        <h1>Laporan Pembayaran</h1>
        <form action="/admin/payment-report" method="GET">
            <div class="row">
                <label for="start" class="col-1 col-form-label">Dari</label>
                <div class="col-3">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">
                </div>
                <label for="end" class="col-1 col-form-label">Sampai</label>
                <div class="col-3">
                    <input type="date" class="form-control" id="end" name="end" value="{{ $end }}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success">Cari</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Siswa</th>
                    <th>Tanggal Bayar</th>
                    <th>Jumlah</th>
                    <th>SPP</th>
                    <th>Metode</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($payment_list as $payment)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $payment->bill->student->name }}</td>
                        <td>{{ $payment->date }}</td>
                        <td>{{ $payment->amount }}</td>
                        <td>Bulan {{ $payment->bill->month }} {{ $payment->bill->year }}</td>
                        <td>{{ $payment->method }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-secondary" onclick="window.print()">
            Print
        </button>
    </div>
@endsection
