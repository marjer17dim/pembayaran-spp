@extends('app')

@section('content')
    <div class="container">
        <h1>Tambah Pembayaran</h1>
        <form action="/admin/payments" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                <div class="col-4 mb-3">
                    <label for="amount" class="form-label">Jumlah</label>
                    <input type="number" class="form-control" id="amount" name="amount">
                </div>
                <div class="col-4 mb-3">
                    <label for="date" class="form-label">Tanggal</label>
                    <input type="date" class="form-control" id="date" name="date">
                </div>
                <div class="col-4 mb-3">
                    <label class="form-label">Metode</label>
                    @foreach (['cash', 'transfer'] as $item)
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="method" value="{{ $item }}">
                            <label class="form-check-label">{{ $item }}</label>
                        </div>
                    @endforeach
                </div>
                <div class="col-3 mb-3">
                    <label for="proof_file" class="form-label">Bukti</label>
                    <input type="file" class="form-control" id="proof_file" name="proof_file"
                        accept="image/png,image/jpeg">
                </div>
                <div class="col-4 mb-3">
                    <label class="form-label">Bill ID</label>
                    <select name="bill_id" class="form-select">
                        @foreach ($bill_list as $bill)
                            <option value="{{ $bill->id }}">{{ $bill->id }} - {{ $bill->student->name }} - Bulan
                                {{ $bill->month }} {{ $bill->year }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
