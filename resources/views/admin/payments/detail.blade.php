@extends('app')

@section('content')
    <div class="container">
        <h1>Detail Pembayaran</h1>
        <form action="/admin/payments/{{ $payment->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-4 mb-3">
                    <label for="amount" class="form-label">Jumlah</label>
                    <input type="number" class="form-control" id="amount" name="amount" value="{{ $payment->amount }}">
                </div>
                <div class="col-4 mb-3">
                    <label for="date" class="form-label">Tanggal</label>
                    <input type="date" class="form-control" id="date" name="date" value="{{ $payment->date }}">
                </div>
                <div class="col-4 mb-3">
                    <label class="form-label">Metode</label>
                    @foreach (['cash', 'transfer'] as $item)
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="method" value="{{ $item }}"
                                {{ $payment->method == $item ? 'checked' : '' }}>
                            <label class="form-check-label">{{ $item }}</label>
                        </div>
                    @endforeach
                </div>
                <div class="col-4 mb-3">
                    <label for="proof_file" class="form-label">Bukti</label>
                    <input type="file" class="form-control" id="proof_file" name="proof_file"
                        accept="image/png,image/jpeg">
                    <img src="{{ asset('storage/' . $payment->proof_file) }}" style="width: 300px">
                </div>
                <div class="col-4 mb-3">
                    <label class="form-label">Bill ID</label>
                    <select name="bill_id" class="form-select" disabled>
                        <option value="{{ $payment->bill_id }}">{{ $payment->bill_id }} -
                            {{ $payment->bill->student->name }} Bulan {{ $payment->bill->month }}
                            {{ $payment->bill->year }}</option>
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
