@extends('app')

@section('content')
    <div class="container">
        <h1>Data Pembayaran</h1>
        <p>{{ $payment_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tanggal</th>
                    <th>Jumlah</th>
                    <th>Bill ID</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($payment_list as $payment)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $payment->date }}</td>
                        <td>{{ $payment->amount }}</td>
                        <td>{{ $payment->bill_id }}</td>
                        <td>
                            <a href="#" class="btn btn-primary">Detail</a>
                            <a href="#" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
