@extends('app')

@section('content')
    <div class="container">
        <h1>Data Tagihan</h1>
        <p>{{ $bill_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Bulan Tahun</th>
                    <th>Jumlah</th>
                    <th>Student ID</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($bill_list as $bill)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $bill->month }} {{ $bill->year }}</td>
                        <td>{{ $bill->amount }}</td>
                        <td>{{ $bill->student_id }}</td>
                        <td>
                            <a href="#" class="btn btn-primary">Detail</a>
                            <a href="#" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
