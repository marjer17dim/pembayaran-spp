<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;

class SummaryController extends Controller
{
    public function paymentReport(Request $request)
    {
        $data = $request->validate([
            'start' => 'filled',
            'end' => 'filled'
        ]);

        if (array_key_exists('start', $data) && array_key_exists('end', $data)) {
            $payments = Payment::where('date', '>=', $data['start'])
                ->where('date', '<=', $data['end'])->get();
            $start = $data['start'];
            $end = $data['end'];
        } else {
            $payments = Payment::all();
            $start = '';
            $end = '';
        }

        return view('admin.payments.summary', [
            'payment_list' => $payments, 'start' => $start, 'end' => $end
        ]);
    }
}
