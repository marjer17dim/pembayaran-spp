<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Bill;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::paginate(5);
        return view('admin.payments.index', ['payment_list' => $payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bills = Bill::all();
        return view('admin.payments.create', ['bill_list' => $bills]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'amount' => 'required',
            'date' => 'required',
            'method' => 'required',
            'proof_file' => 'required|file',
            'bill_id' => 'required'
        ]);

        $path = $request->proof_file->store('public/images');
        $data['proof_file'] = str_replace('public/', '', $path);

        Payment::create($data);

        return redirect('/admin/payments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = Payment::find($id);
        return view('admin.payments.detail', ['payment' => $payment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'amount' => 'required',
            'date' => 'required',
            'method' => 'required',
            'proof_file' => 'nullable|file'
        ]);

        if (array_key_exists('proof_file', $data)) {
            $path = $request->proof_file->store('public/images');
            $data['proof_file'] = str_replace('public/', '', $path);
        }

        Payment::where('id', $id)->update($data);

        return redirect('/admin/payments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
