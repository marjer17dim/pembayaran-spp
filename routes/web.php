<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\StudentController as AdminStudentController;
use App\Http\Controllers\Admin\BillController as AdminBillController;
use App\Http\Controllers\Admin\PaymentController as AdminPaymentController;
use App\Http\Controllers\Admin\SummaryController as AdminSummaryController;

use App\Http\Controllers\Staff\UserController as StaffUserController;
use App\Http\Controllers\Staff\StudentController as StaffStudentController;
use App\Http\Controllers\Staff\BillController as StaffBillController;
use App\Http\Controllers\Staff\PaymentController as StaffPaymentController;

use App\Http\Controllers\Student\BillController as StudentBillController;
use App\Http\Controllers\Student\PaymentController as StudentPaymentController;

use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::middleware(['level:admin'])->group(function () {
        Route::get('/admin', function () {
            return view('admin.home');
        });
        Route::resource('/admin/users', AdminUserController::class);
        Route::resource('/admin/students', AdminStudentController::class);
        Route::resource('/admin/bills', AdminBillController::class);
        Route::resource('/admin/payments', AdminPaymentController::class);
        Route::get('/admin/payment-report', [AdminSummaryController::class, 'paymentReport']);
    });

    Route::middleware(['level:staff'])->group(function () {
        Route::get('/staff', function () {
            return view('staff.home');
        });
        Route::resource('/staff/users', StaffUserController::class);
        Route::resource('/staff/students', StaffStudentController::class);
        Route::resource('/staff/bills', StaffBillController::class);
        Route::resource('/staff/payments', StaffPaymentController::class);
    });

    Route::middleware(['level:student'])->group(function () {
        Route::get('/student', function () {
            return view('student.home');
        });
        Route::resource('/student/bills', StudentBillController::class);
        Route::resource('/student/payments', StudentPaymentController::class);
    });
});

Route::get('/login', [LoginController::class, 'show'])->name('login');

Route::post('/login', [LoginController::class, 'check']);

Route::get('/logout', [LoginController::class, 'logout']);
