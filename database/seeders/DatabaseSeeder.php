<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Bill;
use App\Models\Payment;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user_1 = User::create([
            'username' => 'agustinus',
            'password' => Hash::make('Coba_123'),
            'level' => 'student'
        ]);

        $user_2 = User::create([
            'username' => 'amelia',
            'password' => Hash::make('Coba_124'),
            'level' => 'student'
        ]);

        $user_3 = User::create([
            'username' => 'christian',
            'password' => Hash::make('Coba_125'),
            'level' => 'student'
        ]);

        $user_4 = User::create([
            'username' => 'christopher',
            'password' => Hash::make('Coba_126'),
            'level' => 'student'
        ]);

        $user_5 = User::create([
            'username' => 'dinda',
            'password' => Hash::make('Coba_127'),
            'level' => 'student'
        ]);

        User::create([
            'username' => 'marvin',
            'password' => Hash::make('Coba_123'),
            'level' => 'admin'
        ]);

        User::create([
            'username' => 'jerremy',
            'password' => Hash::make('Coba_123'),
            'level' => 'staff'
        ]);

        $student_1 = Student::create([
            'nisn' => '0043699212',
            'nis' => '202110001',
            'name' => 'Agustinus Maruli Tua',
            'address' => 'Jalan Sukamulya no. 13',
            'phone' => '081220636437',
            'user_id' => $user_1->id
        ]);

        $student_2 = Student::create([
            'nisn' => '0053285771',
            'nis' => '202110002',
            'name' => 'Amelia Venesa',
            'address' => 'Jalan Cibangkong no. 101',
            'phone' => '0895356012314',
            'user_id' => $user_2->id
        ]);

        $student_3 = Student::create([
            'nisn' => '0045915172',
            'nis' => '202110003',
            'name' => 'Christian Andrean',
            'address' => 'Jalan Cikambuy Hilir no. 19',
            'phone' => '082211423244',
            'user_id' => $user_3->id
        ]);

        $student_4 = Student::create([
            'nisn' => '0032851796',
            'nis' => '202110004',
            'name' => 'Christopher Rafael Gunawan',
            'address' => 'Jalan Irigasi, Gg. Laksana Blk no. 73',
            'phone' => '085776676376',
            'user_id' => $user_4->id
        ]);

        $student_5 = Student::create([
            'nisn' => '0047941273',
            'nis' => '202110005',
            'name' => 'Dinda Maryati Simbolon',
            'address' => 'Warung Muncang',
            'phone' => '082219720961',
            'user_id' => $user_5->id
        ]);

        $bill_1 = Bill::create([
            'amount' => 400000,
            'month' => 8,
            'year' => 2022,
            'student_id' => $student_1->id
        ]);

        $bill_2 = Bill::create([
            'amount' => 400000,
            'month' => 8,
            'year' => 2022,
            'student_id' => $student_2->id
        ]);

        $bill_3 = Bill::create([
            'amount' => 400000,
            'month' => 8,
            'year' => 2022,
            'student_id' => $student_3->id
        ]);

        $bill_4 = Bill::create([
            'amount' => 400000,
            'month' => 8,
            'year' => 2022,
            'student_id' => $student_4->id
        ]);

        $bill_5 = Bill::create([
            'amount' => 400000,
            'month' => 8,
            'year' => 2022,
            'student_id' => $student_5->id
        ]);

        Payment::create([
            'amount' => 400000,
            'date' => '2022-08-03',
            'method' => 'cash',
            'proof_file' => '',
            'bill_id' => $bill_1->id
        ]);

        Payment::create([
            'amount' => 380000,
            'date' => '2022-08-01',
            'method' => 'transfer',
            'proof_file' => '',
            'bill_id' => $bill_2->id
        ]);

        Payment::create([
            'amount' => 400000,
            'date' => '2022-08-01',
            'method' => 'transfer',
            'proof_file' => '',
            'bill_id' => $bill_3->id
        ]);

        Payment::create([
            'amount' => 350000,
            'date' => '2022-08-05',
            'method' => 'cash',
            'proof_file' => '',
            'bill_id' => $bill_4->id
        ]);

        Payment::create([
            'amount' => 400000,
            'date' => '2022-08-05',
            'method' => 'cash',
            'proof_file' => '',
            'bill_id' => $bill_5->id
        ]);
    }
}
